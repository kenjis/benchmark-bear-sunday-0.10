<?php

namespace My\Hello\Resource\Page;

use BEAR\Resource\ResourceObject;

class Hello extends ResourceObject
{
    public function onGet($name = 'World')
    {
        $this->body = 'Hello ' . $name . '!';
        return $this;
    }
}
