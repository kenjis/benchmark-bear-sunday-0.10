# BEAR.Sunday 0.10 for Benchmarking

## Installation

~~~
$ mkdir ~/tmp
$ cd ~/tmp
$ git clone git@bitbucket.org:kenjis/benchmark-bear-sunday-0.10.git
$ cd benchmark-bear-sunday-0.10
$ composer install
~~~

~~~
$ chmod o+w var/tmp
~~~

~~~
$ cd /path/to/htdocs
$ ln -s ~/tmp/benchmark-bear-sunday-0.10/var/www/ bear
~~~

> Note: You can't change folder name "bear".

## Benchmarking

~~~
$ siege -b -c 10 -t 3S http://localhost/bear/hello?name=BEAR
~~~
